/*
 * stimer.h
 *
 *  Created on: 24Oct.,2018
 *      Author: jmoschini
 */

#ifndef STIMER_H_
#define STIMER_H_

#include <stdbool.h>

#include "list.h"

typedef struct _stimer_
{
	LIST_ITEM list;
	int count;
	bool removed;

} STIMER_STRUCT;



extern void STimer_Init( void );
extern bool STimer_Initial(STIMER_STRUCT *st);
extern bool STimer_Add(STIMER_STRUCT *st, int cnt);
extern bool STimer_Finished(STIMER_STRUCT *st);
extern void STimer_Tick(void);

#endif /* STIMER_H_ */
