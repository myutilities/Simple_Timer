/*
 * sme_timer.c
 *
 *  Created on: 24Oct.,2018
 *      Author: jmoschini
 */


/*
 * high level timer control for event triggers
 */
#include <stdbool.h>
#include <stdio.h>
#include "stm32l4xx_hal.h"
#include "list.h"
#include "simple_timer.h"

/* keep a current time */
LIST_ITEM *ctime;
LIST_ITEM *atime;	/* add time structures */


/**
 * @fn void STimer_Tick(void)
 * @brief call this externally to tick timmer
 *
 */
void STimer_Tick(void)
{
	LIST_ITEM *alist= 0;
	LIST_ITEM *blist= 0;
	STIMER_STRUCT *st;


	while(ctime != NULL)
	{
		alist = ctime;
		ctime = List_RemoveItem(ctime,alist);
		if((st = (STIMER_STRUCT *)alist->owner) != NULL)
		{
			if( st->count != 0)	st->count--;

			if(st->count == 0)
			{
				st->removed = true;
			}
			else
			{
				blist = List_AddItem(blist,alist);
			}
		}

	}

	while(blist != NULL)
	{
	   if(ctime == NULL) /* we are expecting ctime to be empty */
	   {
	      ctime = blist; /* this will copy everything */
	      blist = NULL;
	   }
	   else
	   {
	      alist = blist;
	      blist = List_RemoveItem(blist,alist);
	      ctime = List_AddItem(ctime,alist);
	   }
	}

	while(atime != NULL)
	{
		alist = atime;
		atime = List_RemoveItem(atime,alist);
		ctime = List_AddItem(ctime,alist);
	}
}


/* add the timer to the list */
bool STimer_Add(STIMER_STRUCT *st, int cnt)
{
	bool retval = false;

	if(st !=NULL)
	{
		if(st->removed)
		{
			st->list.owner = st;
			st->count = cnt;
			st->removed = false;
			HAL_SuspendTick();
			atime =List_AddItem(atime,&st->list);
			HAL_ResumeTick();
			retval = true;
		}
	}
	return(retval);
}


/*
 * @brief inialise or set the defaults
 */
bool STimer_Initial(STIMER_STRUCT *st)
{
	bool retval = false;

	if(st != NULL)
	{
		retval = true;
		st->list.owner = st;
		st->count = 0;
		st->removed = true;
	}
	return(retval);
}

/*
 * @brief  Test to see if timer is zero and removed
 */
bool STimer_Finished(STIMER_STRUCT *st)
{
	bool retval = false;

	if(st != NULL)
	{
		if(st->count == 0)
		{
			if(st->removed)
			{
				retval = true;
			}
		}
	}
	return(retval);
}

/*
 * initilse the time
 */
void STimer_Init( void )
{
	ctime = NULL;
	atime = NULL;
}
